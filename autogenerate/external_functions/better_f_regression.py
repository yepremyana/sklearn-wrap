"""
Function copied from https://gitlab.datadrivendiscovery.org/d3m/datasets/blob/master/seed_datasets_current/534_cps_85_wages/534_cps_85_wages_solution/modules/feature_selection.py#L18
"""

import numpy as np
from scipy import stats
from scipy.linalg import norm
from scipy.sparse import issparse
from sklearn.utils import check_X_y
from sklearn.utils.extmath import row_norms, safe_sparse_dot


def better_f_regression(X, y, center=True):
    """Univariate linear regression tests.

    Quick linear model for testing the effect of a single regressor,
    sequentially for many regressors.

    This is done in 2 steps:

    1. The cross correlation between each regressor and the target is computed,
       that is, ((X[:, i] - mean(X[:, i])) * (y - mean_y)) / (std(X[:, i]) *
       std(y)).
    2. It is converted to an F score then to a p-value.

    Read more in the :ref:`User Guide <univariate_feature_selection>`.

    Parameters
    ----------
    X : {array-like, sparse matrix}  shape = (n_samples, n_features)
        The set of regressors that will be tested sequentially.

    y : array of shape(n_samples).
        The data matrix

    center : True, bool,
        If true, X and y will be centered.

    Returns
    -------
    F : array, shape=(n_features,)
        F values of features.

    pval : array, shape=(n_features,)
        p-values of F-scores.

    See also
    --------
    f_classif: ANOVA F-value between label/feature for classification tasks.
    chi2: Chi-squared stats of non-negative features for classification tasks.
    """
    X, y = check_X_y(X, y, ['csr', 'csc', 'coo'], dtype=np.float64)
    n_samples = X.shape[0]

    if center:
        y = y - np.mean(y)
        if issparse(X):
            X_means = X.mean(axis=0).getA1()
        else:
            X_means = X.mean(axis=0)
        X_norms = np.sqrt(row_norms(X.T, squared=True) - n_samples*X_means**2)
    else:
        X_norms = row_norms(X.T)

    # compute the correlation
    corr = safe_sparse_dot(y, X)
    corr /= X_norms
    corr /= norm(y)

    # convert to p-value
    degrees_of_freedom = y.size - (2 if center else 1)
    F = corr ** 2 / (1 - corr ** 2) * degrees_of_freedom
    pv = stats.f.sf(F, 1, degrees_of_freedom)
    return F, pv
