{% extends "base.template" %}
{% block interface %}from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import ProbabilisticCompositionalityMixin{% endblock %}
{% block return_result %}return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='append',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
    ){% endblock %}


{% block classname %}class SK{{ class_name }}(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]{% if contains_log_proba or contains_proba %},
                          ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams]{% endif %}):{% endblock %}
    {% block class_variables %}
        self._training_inputs = None
        self._training_outputs = None
        self._target_names = None
        self._training_indices = None
        self._fitted = False{% endblock %}

    {% block set_training_data %}
    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs, self._training_indices = self._get_columns_to_fit(inputs, self.hyperparams)
        self._training_outputs, self._target_names = self._get_targets(outputs, self.hyperparams)
        self._fitted = False
        {% endblock %}

    {% block fit %}
    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")
        sk_training_output = d3m_ndarray(self._training_outputs)

        shape = sk_training_output.shape
        if len(shape) == 2 and shape[1] == 1:
            sk_training_output = numpy.ravel(sk_training_output)

        self._clf.fit(self._training_inputs, sk_training_output)
        self._fitted = True

        return CallResult(None)
        {% endblock %}
    {% block produce %}
    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        sk_inputs = inputs
        if self.hyperparams['use_semantic_types']:
            sk_inputs = inputs.iloc[:, self._training_indices]
        sk_output = self._clf.predict(sk_inputs)
        if sparse.issparse(sk_output):
            sk_output = sk_output.toarray()
        output = d3m_dataframe(sk_output, generate_metadata=False, source=self)
        output.metadata = inputs.metadata.clear(source=self, for_value=output, generate_metadata=True)
        output.metadata = self._add_target_semantic_types(metadata=output.metadata, target_names=self._target_names, source=self)
        if not self.hyperparams['use_semantic_types']:
            return CallResult(output)
        outputs = common_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                               add_index_columns=self.hyperparams['add_index_columns'],
                                               inputs=inputs, column_indices=self._training_indices, columns_list=[output], source=self)

        return CallResult(outputs)
        {% endblock %}
{% block get_set_params %}{% if params|length == 0 %}
    def get_params(self) -> Params:
        """
        A noop.
        """
        return None

    def set_params(self, *, params: Params) -> None:
        """
        A noop.
        """
        self._fitted = True
        return
{% else %}
    def get_params(self) -> Params:
        if not self._fitted:
            return Params({% for param in params.keys() %}
                {{ param }}=None,{% endfor %}
                training_indices_=self._training_indices,
                target_names_=self._target_names
            )

        return Params({% for param in params.keys() %}
            {{ param }}=getattr(self._clf, '{{ param }}', None),{% endfor %}
            training_indices_=self._training_indices,
            target_names_=self._target_names
        )

    def set_params(self, *, params: Params) -> None:{% for param in params.keys() %}
        self._clf.{{ param }} = params['{{ param }}']{% endfor %}
        self._training_indices = params['training_indices_']
        self._target_names = params['target_names_']
        self._fitted = False
        {% for param in params.keys() %}
        if params['{{ param }}'] is not None:
            self._fitted = True{% endfor %}{% if params|length ==0 %}pass{% endif %}{% endif %}{% endblock %}
{% block log_likelihoods %}{% if contains_log_proba %}
    def log_likelihoods(self, *,
                    outputs: Outputs,
                    inputs: Inputs,
                    timeout: float = None,
                    iterations: int = None) -> CallResult[Sequence[float]]:
        inputs = inputs.values  # Get ndarray
        outputs = outputs.values
        return CallResult(self._clf.predict_log_proba(inputs)[:, outputs]){%  endif %}
    {% if contains_proba %}
    def log_likelihoods(self, *,
                    outputs: Outputs,
                    inputs: Inputs,
                    timeout: float = None,
                    iterations: int = None) -> CallResult[Sequence[float]]:
        inputs = inputs.values  # Get ndarray
        outputs = outputs.values
        return CallResult(numpy.log(self._clf.predict_proba(inputs)[:, outputs])){% endif %}
{% endblock %}
