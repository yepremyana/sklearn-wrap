import unittest
import numpy as np
import pickle
from typing import NamedTuple

from sklearn import datasets
from sklearn.utils import shuffle
import scipy.sparse

from autogenerate.d3m_sklearn_wrap.sklearn_wrap import {{ class_name }}
from tests.params_check import params_check
from d3m.metadata import base as metadata_base
from d3m import container
from pandas.util.testing import assert_frame_equal

# Common random state
rng = np.random.RandomState(0)

{%- if test_data == None %}
{% set test_data = "iris" %}{% endif %}
{% include "test_data.template" %}


class Test{{ class_name }}(unittest.TestCase):
    def fit(self, hyperparams):
        clf = {{ class_name }}.{{ class_name }}(hyperparams=hyperparams)
        {% block custom_setup %}
        {% endblock %}

        assert_frame_equal(first_output.value, output.value)
        assert_frame_equal(new_output.value, output.value)
        # We want to test the running of the code without errors and not the correctness of it
        # since that is assumed to be tested by sklearn

    def test_with_semantic_types(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults().replace({"use_semantic_types": True})
        self.fit(hyperparams)

    def test_without_semantic_types(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults()
        self.fit(hyperparams)

    {% block test_params %}def test_params(self):
        params_check({{ class_name }}.{{ class_name }}, train_set)
    {% endblock %}
