# Introduction

The `master` and `devel` branches of this repository contains the scripts that generate the sklearn wrapper code using Jinja2 templates. The wrapper code follows the [D3M TA 1 Python interface](.https://gitlab.com/datadrivendiscovery/d3m#python-interfaces-for-ta1-primitives).

Generated primitives are build automatically and pushed to [`dist`](https://gitlab.com/datadrivendiscovery/sklearn-wrap/tree/dist) and [`dev-dist`](https://gitlab.com/datadrivendiscovery/sklearn-wrap/tree/dev-dist) branches of this repository.

## Installation

To install dependencies for generator scripts:

```
$ pip install --process-dependency-links -r requirements.txt
```

To install stable version of generated primitives from repository:

```
$ pip install --process-dependency-links git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@dist
```

Or version from `devel` branch development version:

```
$ pip install --process-dependency-links git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@dev-dist
```

## Generation

The auto generation in broken down into two steps.

### Curation: 

We need to augment the sklearn base classes with some additional metadata about the hyperparameters. 
This additional metadata is captured in an overlay file. You can find the overlay files in the tests/resources directory. 

To start curating a new sklearn class, you need to first generate the template. 
Run 

```
$ python -m autogenerate.generate_overlay_template <sklearn class name> <overlay file>
```

This will create a new parsed json in the directory - [`tests/resources/primitive-jsons`](./tests/resources/primitive-jsons) and create a blank template in the overlay file for the user to curate. 


### Generation:

Now to generate the wrapped code, run [`autogenerate/wrap_sklearn.py`](./autogenerate/wrap_sklearn.py).


```
$ python -m autogenerate.wrap_sklearn --dir tests/resources/primitive-jsons --overlays tests/resources/overlay_classifiers.json tests/resources/overlay_regressors.json tests/resources/overlay_preprocessors.json --module_config autogenerate/config/setup.config.json
```

- This script should create a directory `d3m_sklearn_wrap` with the `setup.py` file in the `autogenerate` directory.

- It will also generate individual test cases for each class and place it in the `tests` directory. 

If the user does not want to generate the entire `d3m_sklearn_wrap` module directory but only generate the code for
the classes to test, they can run the [`autogenerate/autogen_supervised.py`](autogenerate/autogen_supervised.py) script
which will generate the output directory with the classes which are present in the overlay file.


## Tests

You can run generated tests by running `run_tests.sh`.

Note: To run the tests, it is assumed that the user has the `d3m_skearn_wrap` directory within the `autogenerate` dir. 
