{
  "name": "sklearn.preprocessing.data.RobustScaler",
  "id": "7a0d98e507ae5e5166153eedcd6a4605",
  "common_name": "RobustScaler",
  "is_class": true,
  "tags": [
    "preprocessing",
    "data"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/preprocessing/data.py#L939",
  "parameters": [
    {
      "type": "boolean",
      "name": "with_centering",
      "description": "If True, center the data before scaling. This will cause ``transform`` to raise an exception when attempted on sparse matrices, because centering them entails building a dense matrix which in common use cases is likely to be too large to fit in memory. "
    },
    {
      "type": "boolean",
      "name": "with_scaling",
      "description": "If True, scale the data to interquartile range. "
    },
    {
      "type": "tuple",
      "name": "quantile_range",
      "description": "Default: (25.0, 75.0) = (1st quantile, 3rd quantile) = IQR Quantile range used to calculate ``scale_``.  .. versionadded:: 0.18 "
    }
  ],
  "attributes": [
    {
      "type": "array",
      "name": "center_",
      "description": "The median value for each feature in the training set. "
    },
    {
      "type": "array",
      "name": "scale_",
      "description": "The (scaled) interquartile range for each feature in the training set.  .. versionadded:: 0.17 *scale_* attribute.  See also -------- robust_scale: Equivalent function without the estimator API.  :class:`sklearn.decomposition.PCA` Further removes the linear correlation across features with 'whiten=True'. "
    }
  ],
  "description": "\"Scale features using statistics that are robust to outliers.\n\nThis Scaler removes the median and scales the data according to\nthe quantile range (defaults to IQR: Interquartile Range).\nThe IQR is the range between the 1st quartile (25th quantile)\nand the 3rd quartile (75th quantile).\n\nCentering and scaling happen independently on each feature (or each\nsample, depending on the ``axis`` argument) by computing the relevant\nstatistics on the samples in the training set. Median and  interquartile\nrange are then stored to be used on later data using the ``transform``\nmethod.\n\nStandardization of a dataset is a common requirement for many\nmachine learning estimators. Typically this is done by removing the mean\nand scaling to unit variance. However, outliers can often influence the\nsample mean / variance in a negative way. In such cases, the median and\nthe interquartile range often give better results.\n\n.. versionadded:: 0.17\n\nRead more in the :ref:`User Guide <preprocessing_scaler>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.preprocessing.data.RobustScaler.fit",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The data used to compute the median and quantiles used for later scaling along the features axis. '"
        }
      ],
      "description": "'Compute the median and quantiles to be used for scaling.\n"
    },
    {
      "name": "fit_transform",
      "id": "sklearn.preprocessing.data.RobustScaler.fit_transform",
      "parameters": [
        {
          "type": "numpy",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training set. "
        },
        {
          "type": "numpy",
          "shape": "n_samples",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "returns": {
        "type": "numpy",
        "shape": "n_samples, n_features_new",
        "name": "X_new",
        "description": "Transformed array.  '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.preprocessing.data.RobustScaler.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "inverse_transform",
      "id": "sklearn.preprocessing.data.RobustScaler.inverse_transform",
      "parameters": [
        {
          "type": "array-like",
          "name": "X",
          "description": "The data used to scale along the specified axis. '"
        }
      ],
      "description": "'Scale back the data to the original representation\n"
    },
    {
      "name": "set_params",
      "id": "sklearn.preprocessing.data.RobustScaler.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    },
    {
      "name": "transform",
      "id": "sklearn.preprocessing.data.RobustScaler.transform",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "The data used to scale along the specified axis. '"
        }
      ],
      "description": "'Center and scale the data.\n\nCan be called on sparse input, provided that ``RobustScaler`` has been\nfitted to dense input and ``with_centering=False``.\n"
    }
  ]
}