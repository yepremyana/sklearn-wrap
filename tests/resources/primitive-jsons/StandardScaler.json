{
  "is_class": true, 
  "library": "sklearn", 
  "compute_resources": {}, 
  "common_name": "Standard Scaler", 
  "id": "0c2d6e43-0d59-338e-889e-652d5114bc55", 
  "category": "preprocessing.data", 
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/preprocessing/data.py#L449", 
  "parameters": [
    {
      "type": "boolean", 
      "name": "with_mean", 
      "description": "If True, center the data before scaling. This does not work (and will raise an exception) when attempted on sparse matrices, because centering them entails building a dense matrix which in common use cases is likely to be too large to fit in memory. "
    }, 
    {
      "type": "boolean", 
      "name": "with_std", 
      "description": "If True, scale the data to unit variance (or equivalently, unit standard deviation). "
    }
  ], 
  "tags": [
    "preprocessing", 
    "data"
  ], 
  "common_name_unanalyzed": "Standard Scaler", 
  "schema_version": 1.0, 
  "languages": [
    "python2.7"
  ], 
  "version": "0.18.1", 
  "build": [
    {
      "type": "pip", 
      "package": "scikit-learn"
    }
  ], 
  "description": "\"Standardize features by removing the mean and scaling to unit variance\n\nCentering and scaling happen independently on each feature by computing\nthe relevant statistics on the samples in the training set. Mean and\nstandard deviation are then stored to be used on later data using the\n`transform` method.\n\nStandardization of a dataset is a common requirement for many\nmachine learning estimators: they might behave badly if the\nindividual feature do not more or less look like standard normally\ndistributed data (e.g. Gaussian with 0 mean and unit variance).\n\nFor instance many elements used in the objective function of\na learning algorithm (such as the RBF kernel of Support Vector\nMachines or the L1 and L2 regularizers of linear models) assume that\nall features are centered around 0 and have variance in the same\norder. If a feature has a variance that is orders of magnitude larger\nthat others, it might dominate the objective function and make the\nestimator unable to learn from other features correctly as expected.\n\nThis scaler can also be applied to sparse CSR or CSC matrices by passing\n`with_mean=False` to avoid breaking the sparsity structure of the data.\n\nRead more in the :ref:`User Guide <preprocessing_scaler>`.\n", 
  "methods_available": [
    {
      "id": "sklearn.preprocessing.data.StandardScaler.fit", 
      "description": "'Compute the mean and std to be used for later scaling.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "The data used to compute the mean and standard deviation used for later scaling along the features axis.  y: Passthrough for ``Pipeline`` compatibility. '"
        }
      ], 
      "name": "fit"
    }, 
    {
      "id": "sklearn.preprocessing.data.StandardScaler.fit_transform", 
      "returns": {
        "shape": "n_samples, n_features_new", 
        "type": "numpy", 
        "name": "X_new", 
        "description": "Transformed array.  '"
      }, 
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "numpy", 
          "name": "X", 
          "description": "Training set. "
        }, 
        {
          "shape": "n_samples", 
          "type": "numpy", 
          "name": "y", 
          "description": "Target values. "
        }
      ], 
      "name": "fit_transform"
    }, 
    {
      "id": "sklearn.preprocessing.data.StandardScaler.get_params", 
      "returns": {
        "type": "mapping", 
        "name": "params", 
        "description": "Parameter names mapped to their values. '"
      }, 
      "description": "'Get parameters for this estimator.\n", 
      "parameters": [
        {
          "type": "boolean", 
          "optional": "true", 
          "name": "deep", 
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ], 
      "name": "get_params"
    }, 
    {
      "id": "sklearn.preprocessing.data.StandardScaler.inverse_transform", 
      "description": "'Scale back the data to the original representation\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The data used to scale along the features axis. '"
        }
      ], 
      "name": "inverse_transform"
    }, 
    {
      "id": "sklearn.preprocessing.data.StandardScaler.partial_fit", 
      "description": "'Online computation of mean and std on X for later scaling.\nAll of X is processed as a single batch. This is intended for cases\nwhen `fit` is not feasible due to very large number of `n_samples`\nor because X is read from a continuous stream.\n\nThe algorithm for incremental mean and std is given in Equation 1.5a,b\nin Chan, Tony F., Gene H. Golub, and Randall J. LeVeque. \"Algorithms\nfor computing the sample variance: Analysis and recommendations.\"\nThe American Statistician 37.3 (1983): 242-247:\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "The data used to compute the mean and standard deviation used for later scaling along the features axis.  y: Passthrough for ``Pipeline`` compatibility. '"
        }
      ], 
      "name": "partial_fit"
    }, 
    {
      "id": "sklearn.preprocessing.data.StandardScaler.set_params", 
      "returns": {
        "name": "self", 
        "description": "\""
      }, 
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n", 
      "parameters": [], 
      "name": "set_params"
    }, 
    {
      "id": "sklearn.preprocessing.data.StandardScaler.transform", 
      "description": "'Perform standardization by centering and scaling\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The data used to scale along the features axis. '"
        }
      ], 
      "name": "transform"
    }
  ], 
  "learning_type": [
    "supervised"
  ], 
  "task_type": [
    "modeling"
  ], 
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/preprocessing/data.py#L449", 
  "category_unanalyzed": "preprocessing.data", 
  "name": "sklearn.preprocessing.data.StandardScaler", 
  "team": "jpl", 
  "attributes": [
    {
      "shape": "n_features,", 
      "type": "ndarray", 
      "name": "scale_", 
      "description": "Per feature relative scaling of the data.  .. versionadded:: 0.17 *scale_* is recommended instead of deprecated *std_*. "
    }, 
    {
      "shape": "n_features", 
      "type": "array", 
      "name": "mean_", 
      "description": "The mean value for each feature in the training set. "
    }, 
    {
      "shape": "n_features", 
      "type": "array", 
      "name": "var_", 
      "description": "The variance for each feature in the training set. Used to compute `scale_` "
    }, 
    {
      "type": "int", 
      "name": "n_samples_seen_", 
      "description": "The number of samples processed by the estimator. Will be reset on new calls to fit, but increments across ``partial_fit`` calls.  See also -------- scale: Equivalent function without the object oriented API.  :class:`sklearn.decomposition.PCA` Further removes the linear correlation across features with 'whiten=True'."
    }
  ]
}