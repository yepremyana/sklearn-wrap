{
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "Linear SVC",
  "id": "b32e5908-b110-37fd-a5b1-78815ae62046",
  "category": "svm.classes",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/svm/classes.py#L14",
  "parameters": [
    {
      "default": "1.0",
      "type": "float",
      "optional": "true",
      "name": "C",
      "description": "Penalty parameter C of the error term. "
    },
    {
      "type": "string",
      "name": "loss",
      "description": "Specifies the loss function. \\'hinge\\' is the standard SVM loss (used e.g. by the SVC class) while \\'squared_hinge\\' is the square of the hinge loss. "
    },
    {
      "type": "string",
      "name": "penalty",
      "description": "Specifies the norm used in the penalization. The \\'l2\\' penalty is the standard used in SVC. The \\'l1\\' leads to ``coef_`` vectors that are sparse. "
    },
    {
      "type": "bool",
      "name": "dual",
      "description": "Select the algorithm to either solve the dual or primal optimization problem. Prefer dual=False when n_samples > n_features. "
    },
    {
      "default": "1e-4",
      "type": "float",
      "optional": "true",
      "name": "tol",
      "description": "Tolerance for stopping criteria.  multi_class: string, \\'ovr\\' or \\'crammer_singer\\' (default=\\'ovr\\') Determines the multi-class strategy if `y` contains more than two classes. ``\"ovr\"`` trains n_classes one-vs-rest classifiers, while ``\"crammer_singer\"`` optimizes a joint objective over all classes. While `crammer_singer` is interesting from a theoretical perspective as it is consistent, it is seldom used in practice as it rarely leads to better accuracy and is more expensive to compute. If ``\"crammer_singer\"`` is chosen, the options loss, penalty and dual will be ignored. "
    },
    {
      "default": "True",
      "type": "boolean",
      "optional": "true",
      "name": "fit_intercept",
      "description": "Whether to calculate the intercept for this model. If set to false, no intercept will be used in calculations (i.e. data is expected to be already centered). "
    },
    {
      "default": "1",
      "type": "float",
      "optional": "true",
      "name": "intercept_scaling",
      "description": "When self.fit_intercept is True, instance vector x becomes ``[x, self.intercept_scaling]``, i.e. a \"synthetic\" feature with constant value equals to intercept_scaling is appended to the instance vector. The intercept becomes intercept_scaling * synthetic feature weight Note! the synthetic feature weight is subject to l1/l2 regularization as all other features. To lessen the effect of regularization on synthetic feature weight (and therefore on the intercept) intercept_scaling has to be increased. "
    },
    {
      "type": "dict, \\'balanced\\'",
      "optional": "true",
      "name": "class_weight",
      "description": "Set the parameter C of class i to ``class_weight[i]*C`` for SVC. If not given, all classes are supposed to have weight one. The \"balanced\" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))`` "
    },
    {
      "type": "int",
      "name": "verbose",
      "description": "Enable verbose output. Note that this setting takes advantage of a per-process runtime setting in liblinear that, if enabled, may not work properly in a multithreaded context. "
    },
    {
      "type": "int",
      "name": "random_state",
      "description": "The seed of the pseudo random number generator to use when shuffling the data. "
    },
    {
      "type": "int",
      "name": "max_iter",
      "description": "The maximum number of iterations to be run. "
    }
  ],
  "tags": [
    "svm",
    "classes"
  ],
  "common_name_unanalyzed": "Linear SVC",
  "schema_version": 1.0,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "description": "'Linear Support Vector Classification.\n\nSimilar to SVC with parameter kernel=\\'linear\\', but implemented in terms of\nliblinear rather than libsvm, so it has more flexibility in the choice of\npenalties and loss functions and should scale better to large numbers of\nsamples.\n\nThis class supports both dense and sparse input and the multiclass support\nis handled according to a one-vs-the-rest scheme.\n\nRead more in the :ref:`User Guide <svm_classification>`.\n",
  "methods_available": [
    {
      "id": "sklearn.svm.classes.LinearSVC.decision_function",
      "returns": {
        "name": "array, shape=(n_samples,) if n_classes == 2 else (n_samples, n_classes)",
        "description": "Confidence scores per (sample, class) combination. In the binary case, confidence score for self.classes_[1] where >0 means this class would be predicted. '"
      },
      "description": "'Predict confidence scores for samples.\n\nThe confidence score for a sample is the signed distance of that\nsample to the hyperplane.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "name": "decision_function"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.densify",
      "returns": {
        "name": "self: estimator",
        "description": "'"
      },
      "description": "'Convert coefficient matrix to dense array format.\n\nConverts the ``coef_`` member (back) to a numpy.ndarray. This is the\ndefault format of ``coef_`` and is required for fitting, so calling\nthis method is only required on models that have previously been\nsparsified; otherwise, it is a no-op.\n",
      "parameters": [],
      "name": "densify"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.fit",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns self. '"
      },
      "description": "'Fit the model according to the given training data.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Training vector, where n_samples in the number of samples and n_features is the number of features. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "Target vector relative to X "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Array of weights that are assigned to individual samples. If not provided, then each sample is given unit weight. "
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.fit_transform",
      "returns": {
        "shape": "n_samples, n_features_new",
        "type": "numpy",
        "name": "X_new",
        "description": "Transformed array.  '"
      },
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "numpy",
          "name": "X",
          "description": "Training set. "
        },
        {
          "shape": "n_samples",
          "type": "numpy",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "name": "fit_transform"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.predict",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "C",
        "description": "Predicted class label per sample. '"
      },
      "description": "'Predict class labels for samples in X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "name": "predict"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.score",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      },
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.set_params",
      "returns": {
        "name": "self",
        "description": "\""
      },
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "parameters": [],
      "name": "set_params"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.sparsify",
      "returns": {
        "name": "self: estimator",
        "description": "'"
      },
      "description": "'Convert coefficient matrix to sparse format.\n\nConverts the ``coef_`` member to a scipy.sparse matrix, which for\nL1-regularized models can be much more memory- and storage-efficient\nthan the usual numpy.ndarray representation.\n\nThe ``intercept_`` member is not converted.\n\nNotes\n-----\nFor non-sparse models, i.e. when there are not many zeros in ``coef_``,\nthis may actually *increase* memory usage, so use this method with\ncare. A rule of thumb is that the number of zero elements, which can\nbe computed with ``(coef_ == 0).sum()``, must be more than 50% for this\nto provide significant benefits.\n\nAfter calling this method, further fitting with the partial_fit\nmethod (if any) will not work until you call densify.\n",
      "parameters": [],
      "name": "sparsify"
    },
    {
      "id": "sklearn.svm.classes.LinearSVC.transform",
      "returns": {
        "shape": "n_samples, n_selected_features",
        "type": "array",
        "name": "X_r",
        "description": "The input samples with only the selected features. '"
      },
      "description": "'DEPRECATED: Support to use estimators as feature selectors will be removed in version 0.19. Use SelectFromModel instead.\n\nReduce X to its most important features.\n\nUses ``coef_`` or ``feature_importances_`` to determine the most\nimportant features.  For models with a ``coef_`` for each class, the\nabsolute sum over the classes is used.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array",
          "name": "X",
          "description": "The input samples. "
        },
        {
          "default": "None",
          "type": "string",
          "optional": "true",
          "name": "threshold",
          "description": "The threshold value to use for feature selection. Features whose importance is greater or equal are kept while the others are discarded. If \"median\" (resp. \"mean\"), then the threshold value is the median (resp. the mean) of the feature importances. A scaling factor (e.g., \"1.25*mean\") may also be used. If None and if available, the object attribute ``threshold`` is used. Otherwise, \"mean\" is used by default. "
        }
      ],
      "name": "transform"
    }
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/svm/classes.py#L14",
  "category_unanalyzed": "svm.classes",
  "name": "sklearn.svm.classes.LinearSVC",
  "team": "jpl",
  "attributes": [
    {
      "shape": "n_features",
      "type": "array",
      "name": "coef_",
      "description": "Weights assigned to the features (coefficients in the primal problem). This is only available in the case of a linear kernel.  ``coef_`` is a readonly property derived from ``raw_coef_`` that follows the internal memory layout of liblinear. "
    },
    {
      "shape": "1",
      "type": "array",
      "name": "intercept_",
      "description": "Constants in decision function. "
    }
  ]
}
